from openai import OpenAI
import streamlit as st
from dotenv import load_dotenv

# Load your .env file to access the API key
load_dotenv()

client = OpenAI()


# Function to get a response from OpenAI
def get_response(job_position, question_type):
    session_prompt = (
        "You are a recruiter for"
        + job_position
        + "positions at one of the leading firms in that industry." +
        "You are to generate interview questions for the candidates."
    )

    if question_type == "behavioral":
        prompt = (
            session_prompt
            + "Generate one typical behavioral interview question for "
            + job_position
            + ". Only show one question in your response."
        )
    else:
        prompt = (
            session_prompt
            + "Generate one typical technical interview question for "
            + job_position
            + ". Only show one question in your response."
        )

    response = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": prompt},
            {"role": "user", "content": question_type},
        ],
    )
    return response.choices[0].message.content


# App title and description
st.image("logo/Duke-MIDS-logo-social.jpg", width=100)  # Optional: Add a logo
st.title("🚀 Interview Question Generator")
st.markdown(
"""
This app generates behavioral and technical interview questions tailored 
to the job position you're interested in. 
Perfect for recruiters, HR professionals, or candidates preparing for interviews!
"""
)

# Styling
st.markdown("---")
st.header("Let's get started!")

# User input for the job position
job_position = st.text_input("📝 Enter the desired job position:", "Data Scientist")

col1, col2 = st.columns(2)
with col1:
    if st.button("🧠 Generate Behavioral Question"):
        question = get_response(job_position, "behavioral")
        st.success("Behavioral Question:")
        st.info(question)

with col2:
    if st.button("💻 Generate Technical Question"):
        question = get_response(job_position, "technical")
        st.success("Technical Question:")
        st.info(question)

# Footer
st.markdown("---")
st.markdown(
    """
Built with ❤️ by [Duke MIDS](https://mids.duke.edu/).
"""
)
