import os
from dotenv import load_dotenv
from openai import OpenAI

# Load environment variables
load_dotenv()


def test_api_key_loaded():
    """Check if the OpenAI API key is loaded from .env"""
    api_key = os.getenv(
        "OPENAI_API_KEY"
    )  # Ensure this matches the key name in your .env
    assert api_key is not None


def test_logo_file_exists():
    """Check if the logo file exists in the specified path"""
    logo_path = "logo/Duke-MIDS-logo-social.jpg"
    assert os.path.exists(logo_path)


def test_openai_api_authentication():
    """Attempt to authenticate with the OpenAI API using the loaded API key"""
    client = OpenAI()  # Assuming this setup works with just environment variable
    try:
        # Attempt to create a small test completion to verify the API key works
        response = client.chat.completions.create(
            model="gpt-3.5-turbo",
            messages=[
                {"role": "system", "content": "You are a general GPT"},
                {"role": "user", "content": "Say yes if you're working"},
            ],
        )
        assert response is not None
    except Exception as e:
        assert False, f"OpenAI API authentication failed: {str(e)}"
