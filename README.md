# IDS 721 Mini Proj 9 [![pipeline status](https://gitlab.com/jaxonyue/ids-721-mini-proj-9/badges/main/pipeline.svg)](https://gitlab.com/jaxonyue/ids-721-mini-proj-9/-/commits/main)

## Overview
* This repository includes the components for **Mini-Project 9 - Streamlit App with LLM Model**

## Goal
* Create a website using Streamlit
* Connect to an open source LLM
* Deploy model via Streamlit or other service

## Streamlit App Introduction
* Link to the Streamlit App: (http://52.71.236.227:8501)
* The web app that I built, InterviewQ, uses OpenAI API to call the GPT 3.5 Turbo LLM, and it has the following features:
  * A `Job Position Text Input` - This field allows users to input a desired job position that they are interested in
  * A `Behavioral Question` button - This button generates a relevant behavioral question for the job position
  * A `Technical Question` button - This button generates a relevant technical question for the job position
* Users can keep generating new questions to help them get practiced on a variety of scenarios
* The GPT model has been meticulously fine-tuned to replicate the role of a recruiter from a top-tier tech company, enhancing the relevance and authenticity of the generated interview questions to closely resemble those used in actual interview settings

## Screenshot of a Generated Behavioral Question
![Behavioral_Q](/uploads/b3f841c1ddb95a5f3aaa2b924bf89339/Behavioral_Q.png)

## Screenshot of a Generated Technical Question
![Technical_Q](/uploads/c12ee502ac8f235dbcb2158b66ed4903/Technical_Q.png)

## Chatbot Performance
* The chatbot performs well in generating relevant and authentic interview questions for a variety of job positions
* The questions generated are well-structured and closely resemble those used in actual interview settings
* The chatbot is user-friendly and easy to use, making it a valuable tool for job seekers to practice their interview skills

## Key Steps
1. Get an OpenAI API key from their website, and install the OpenAI Python package using `pip install openai`
2. Create an `.env` file to store the API key, and add it to the `.gitignore` file
3. Install the Streamlit package using `pip install streamlit`
4. Create `main.py` file to write the Streamlit app code
5. Connect to the OpenAI API using the following code:
```
load_dotenv()
client = OpenAI()
```
6. Create a function to call the OpenAI API and generate the interview questions
7. Create the Streamlit app layout
8. Test run the Streamlit app using `streamlit run main.py`
9. Push the code to your repo
10. Sign into AWS and create an EC2 instance
11. Launch the instance and use Ubuntu as the operating system
12. Connect to the instance using SSH and set up the environment
13. Install the required packages and dependencies
14. Run `export PATH=~/.local/bin/:$PATH`
15. Run `pip install --upgrade jinja2`
16. Add your `.env` file to the instance
17. Test run the Streamlit app using `streamlit run main.py`
18. Set up a screen session to keep the app running in the background
19. Access the app using the public IP address of the EC2 instance
20. My app can be accessed at the following URL: (http://52.71.236.227:8501)


## References
* [OpenAI API Documentation](https://platform.openai.com/docs/overview)
* [Streamlit Documentation](https://docs.streamlit.io/)